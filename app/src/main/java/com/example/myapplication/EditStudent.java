package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.myapplication.model.Model;
import com.example.myapplication.model.Student;

public class EditStudent extends AppCompatActivity {
    String name = "default name";
    String id = "default id";
    Boolean checked = false;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_student);

        Button saveBtn = findViewById(R.id.editstudent_save_btn);
        Button cancelBtn = findViewById(R.id.editstudent_cancel_btn);
        EditText nameEt = findViewById(R.id.editstudent_name_et);
        EditText idEt = findViewById(R.id.editstudent_id_et);
        CheckBox cb = findViewById(R.id.editstudent_cb);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            name = extras.getString("name");
            id = extras.getString("id");
            checked = extras.getBoolean("checked");
            pos = extras.getInt("pos");
        };
        nameEt.setText(name);
        idEt.setText(id);
        cb.setChecked(checked);


        saveBtn.setOnClickListener(view -> {
            String name = nameEt.getText().toString();
            String id = idEt.getText().toString();
            Boolean checked = cb.isChecked();
            Student st = new Student(name, id, "",checked);
            Model.instance().editStudent(st, pos);

            // update the list in the Student List activity
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        });

        cancelBtn.setOnClickListener(view -> finish());
    }
}


