package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class StudentDetails extends AppCompatActivity {
    String name;
    String id;
    Boolean checked;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_details);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            name = extras.getString("name");
            id = extras.getString("id");
            checked = extras.getBoolean("checked");
            pos = extras.getInt("pos");
            TextView nameTv = findViewById(R.id.studentDetails_name_tv);
            nameTv.setText(name);
            TextView idEt = findViewById(R.id.studentDetails_id_tv);
            idEt.setText(id);
            CheckBox cb = findViewById(R.id.studentDetails_cb);
            cb.setChecked(checked);
        }

        Button backBtn = findViewById(R.id.studentDetails_back_btn);
        Button editStudentBtn = findViewById(R.id.studentDetails_edit_btn);
        editStudentBtn.setOnClickListener(view -> {
            Intent intent = new Intent(this, EditStudent.class);
            intent.putExtra("pos", pos);
            intent.putExtra("name", name);
            intent.putExtra("id",id );
            intent.putExtra("checked", checked);
            intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
            startActivity(intent);
            finish();
        });

        backBtn.setOnClickListener(view -> finish());
    }
}