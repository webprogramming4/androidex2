package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.myapplication.model.Model;
import com.example.myapplication.model.Student;

import java.util.List;

public class StudentRecyclerList extends AppCompatActivity {
    List<Student> students;
    int LAUNCH_SECOND_ACTIVITY = 1;
    StudentRecyclerAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_recycler_list);

        students = Model.instance().getAllStudents();
        RecyclerView list = findViewById(R.id.studentrecycle_list);
        list.setHasFixedSize(true);
        adapter = new StudentRecyclerAdapter();
        list.setLayoutManager(new LinearLayoutManager(this));
        adapter = new StudentRecyclerAdapter();
        list.setAdapter(adapter);

        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                Intent i = new Intent(getApplicationContext(), StudentDetails.class);
                Student s = students.get(pos);
                i.putExtra("pos", pos);
                i.putExtra("name", s.getName());
                i.putExtra("id", s.getId());
                i.putExtra("checked", s.getChecked());
                startActivityForResult(i, LAUNCH_SECOND_ACTIVITY);
            }
        });

        adapter.notifyDataSetChanged();

        Button addStudentBtn = findViewById(R.id.StudentRecyclerList_add_student_btn);
        addStudentBtn.setOnClickListener(view -> {
            Intent intent = new Intent(this,AddStudent.class);
            startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY);
        });
    }

    class StudentViewHolder extends RecyclerView.ViewHolder{
        TextView nameTV;
        TextView idTv;
        CheckBox cb;
        public StudentViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            nameTV = itemView.findViewById(R.id.studentlistrow_name_tv);
            idTv = itemView.findViewById(R.id.studentlistrow_id_tv);
            cb = itemView.findViewById(R.id.studentlistrow_cb);
            cb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int)cb.getTag();
                    Student st = students.get(pos);
                    st.setChecked(cb.isChecked());
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();
                    listener.onItemClick(pos);
                }
            });
        }

        public void bind(Student st, int pos){
            nameTV.setText(st.getName());
            idTv.setText(st.getId());
            cb.setChecked(st.getChecked());
            cb.setTag(pos);
        }
    }
    public interface OnItemClickListener{
        void onItemClick(int pos);
    }

    class StudentRecyclerAdapter extends RecyclerView.Adapter<StudentViewHolder>{
        OnItemClickListener listener;
        void setOnItemClickListener(OnItemClickListener listener){
            this.listener = listener;
        }
        @NonNull
        @Override
        public StudentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            students = Model.instance().getAllStudents();
            View view = getLayoutInflater().inflate(R.layout.student_list_row,parent,false);
            return new StudentViewHolder(view,listener);
        }

        @Override
        public void onBindViewHolder(@NonNull StudentViewHolder holder, int position) {
            Student st = students.get(position);
            holder.bind(st,position);
        }

        @Override
        public int getItemCount() {
            return students.size();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if(resultCode == Activity.RESULT_OK){
                adapter.notifyDataSetChanged();
            }
        }
    }
}