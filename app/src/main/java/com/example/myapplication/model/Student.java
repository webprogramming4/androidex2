package com.example.myapplication.model;

public class Student {
    private String name;
    private String id;
    private String avatarUrl;
    private Boolean checked;

    public Student(String name, String id, String avatarUrl, Boolean checked) {
        this.name = name;
        this.id = id;
        this.avatarUrl = avatarUrl;
        this.checked = checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }
}
