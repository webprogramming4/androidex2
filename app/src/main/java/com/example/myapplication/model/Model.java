package com.example.myapplication.model;

import java.util.LinkedList;
import java.util.List;

public class Model {
    private static final Model _instance = new Model();

    private Model(){
        for (int studentNum=0; studentNum<5; studentNum++){
            Student st = new Student(("name "+ studentNum), ""+ studentNum,"",false);
            addStudent(st);
        }
    }

    public static Model instance(){
        return _instance;
    }

    List<Student> data = new LinkedList<>();

    public List<Student> getAllStudents(){
        return data;
    }
    public void addStudent(Student st){
        data.add(st);
    }
    public void editStudent(Student st, int pos) {
        data.set(pos, st);
    }
}
